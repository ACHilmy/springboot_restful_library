package com.amartek.restful_demo.service;

import java.util.List;

import com.amartek.restful_demo.entity.Buku;
import com.amartek.restful_demo.repository.BukuRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BukuService {
    @Autowired
    private BukuRepository bukuRepository;

    public List<Buku> getAll() {
        return bukuRepository.findAll();
    }
}
