package com.amartek.restful_demo.controller;

import com.amartek.restful_demo.service.BukuService;

import java.util.List;

import com.amartek.restful_demo.entity.Buku;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BukuController {
    private final BukuService bukuServices;

    //Constructor
    public BukuController(BukuService bukuServices) {
        this.bukuServices = bukuServices;
    }

    @GetMapping("/gets")
    public List<Buku> getAllBuku() {
        return bukuServices.getAll();
    }

}
